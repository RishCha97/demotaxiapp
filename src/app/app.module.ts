import { NgModule } from '@angular/core';
import { IonicApp, IonicModule } from 'ionic-angular';
import { MyApp } from './app.component';
import { AddPage } from '../pages/add/add';
import { ChatPage } from '../pages/chat/chat';
import { DriverData } from '../providers/taxi-data';DataService
import { DataService } from '../providers/data-service';
import { TaxiListPage } from '../pages/taxi-list/taxi-list';
import { ShowPage } from '../pages/show/show';

@NgModule({
  declarations: [
    MyApp,
    AddPage,
    TaxiListPage,
    ChatPage,
    ShowPage
  ],
  imports: [
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AddPage,
    TaxiListPage,
    ChatPage,
    ShowPage
  ],
  providers: [DriverData , DataService]
})
export class AppModule {}
