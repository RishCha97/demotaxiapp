import { Component, ViewChild } from '@angular/core';
import { NavController,NavParams } from 'ionic-angular';
import { DataService } from '../../providers/data-service';
import { Device } from 'ionic-native'

@Component({
  selector: 'page-chat',
  templateUrl: 'chat.html'
})
export class ChatPage {

  @ViewChild('chat') chat: any;
  public taxi;
  chatMessage: string = '';
  messages: any = [];
  ToChatData: any;                //with Whom We Are Chatting(the taxi Driver)
  // constructor for the class 
  constructor(public navCtrl: NavController, 
  public navParams: NavParams,
  public dataService: DataService) {
      this.ToChatData = this.navParams.get('taxi');
      //console.log(Device.uuid);
      
     this.dataService.getDocuments(this.ToChatData).then((data) => {
       this.messages = data;
     });
 
 }

  // method to send messages
  sendMessage(): void {
    let message = {
      _id: new Date().toISOString(),
      toName : this.ToChatData.name,
      message: this.chatMessage,
      from: Device.uuid ,
      ano: "1"
    };
    this.dataService.addDocument(message);
    this.chatMessage = '';

  }

  
}
