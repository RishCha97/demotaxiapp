import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { TaxiListPage } from '../taxi-list/taxi-list';

@Component({
  selector: 'page-show',
  templateUrl: 'show.html'
})
export class ShowPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {}

  changeto(){
    this.navCtrl.push(TaxiListPage);
  }

}
