import { Component } from '@angular/core';
import { NavController,NavParams } from 'ionic-angular';
import { DriverData } from '../../providers/taxi-data';
import { ChatPage } from '../chat/chat';
import { Geolocation } from 'ionic-native';

@Component({
  selector: 'page-taxi-list',
  templateUrl: 'taxi-list.html'
})
export class TaxiListPage {
  public cityData;
 // taxiList: Array<{ name: string, city: string, id: string,location :string , image :any}> = [];
  public taxiList;
  lat: any;
  lng: any;
  Element1 = '';               //variable for search
  data: any;                   //for data of locations
  newData: any;
  constructor(public navController: NavController, public navParams: NavParams, public taxiData: DriverData){
    this.getDrivers();
      //console.log(data);
    // this.taxiList=
   //  console.log(this.taxiList);
  }

  getDrivers(){
        this.taxiData.getAllDrivers().then((taxis) => {
        this.taxiList = taxis;
       // console.log(this.taxiList);
          }).then( () => {
            this.location();
        });
  }
  

   // View Forecast for 7 days
  doChat(taxi) {
  //  console.log('View Taxi List');
   // console.log(taxi);
    this.navController.push(ChatPage,{ taxi: taxi });
  }


location(){
    Geolocation.getCurrentPosition().then(resp => {
        this.lat = resp.coords.latitude;
        this.lng = resp.coords.longitude;
console.log(this.lat);
  }).then(()=>{
         this.newData=   this.load();
        // console.log("this is newData");
        // console.log(this.newData);
        });

}

/**
 * Function used for searching Drivers by name
 */
getItems(ev : any){
    
    let value = ev.target.value;

     if (value && value.trim() != this.Element1) {
         this.Element1 = value;
      this.taxiList = this.taxiList.filter((item) => {
        return (item.city.toLowerCase().indexOf(value.toLowerCase()) > -1);
      })
    }
    else {
        this.getDrivers();
    }
}


/**
 * all the following functions are used to calculate distance
 */

 load(){
        return new Promise(resolve => {
            console.log(this.taxiList);
                    
                this.data = this.applyHaversine(this.taxiList);
             //   console.log("check this");
              //  console.log(this.data);
                this.data.sort((locationA, locationB) => {
                     //console.log(locationA.distance - locationB.distance);
                    return locationA.distance - locationB.distance;
                });
                //console.log(this.data);
              //  this.list=this.data;
                resolve(this.data);
            });
 
 
    }
 
    applyHaversine(locations){
       // console.log(locations);

        let usersLocation = {
            lat: this.lat, 
            lng: this.lng
        };
        //console.log("this is usersLocation");
         // console.log(usersLocation);
        locations.map((location) => {
            console.log(location);
            let placeLocation = {
                lat: location.lattitude,
                lng: location.longitude
            };
            console.log(usersLocation);
            console.log(placeLocation);
           // console.log(location.lattitude);
            location.distance = this.getDistanceBetweenPoints(
                usersLocation,
                placeLocation,
                'm'
            ).toFixed(8);
        });
 
        return locations;
    }
 
    getDistanceBetweenPoints(start, end, units){
 
        let earthRadius = {
            miles: 3958.8,
            km: 6371,
            m: 6371000
        };
 
        let R = earthRadius[units || 'miles'];
        let lat1 = start.lat;
        let lon1 = start.lng;
        let lat2 = end.lat;
        let lon2 = end.lng;
 
        let dLat = this.toRad((lat2 - lat1));
        let dLon = this.toRad((lon2 - lon1));

        let a = (Math.sin(dLat / 2) * Math.sin(dLat / 2)) + (Math.cos(this.toRad(lat1)) * Math.cos(this.toRad(lat2)) * Math.sin(dLon / 2) * Math.sin(dLon / 2));
        
        let c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        let d = R * c;
        
       // console.log("he;llo"+d);
        return d;
 
    }
 
    toRad(x){
        return x * Math.PI / 180;
    }
 

}

