import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import PouchDB from 'pouchdb';
import { Device } from 'ionic-native'

@Injectable()
export class DataService {

  fbid: number;
  username: string;
  picture: string;
  db: any;
  data: any;
  cloudantUsername: string;
  cloudantPassword: string;
  remote: string;

  // constructor to initialize the Pouchdb and Ibm Bluemix account
  constructor(public http: Http) {
    this.db = new PouchDB('chats');
    this.cloudantUsername = 'testarefournficeirdshned';
    this.cloudantPassword = 'cc3c8d20f509ed89ec22983227a03ad1a1a87885';
    this.remote = 'https://6323cd5c-ab7a-4f85-8e7f-20a6d97305aa-bluemix.cloudant.com/chats';

    //Set up PouchDB
    let options = {
      live: true,
      retry: true,
      continuous: true,
      // auth: {
      //   username: this.cloudantUsername,
      //   password: this.cloudantPassword
      // }
    };

    this.db.sync(this.remote, options);
  }

  // adding the document
  addDocument(message) {
    let chat = {
      _id: new Date().toISOString(),
      message: message.message,
      to:message.toName,
      from: Device.uuid
    }
    this.db.put(message);
    console.log(chat);
  }

  // get all the documents
  getDocuments(message) {

    return new Promise(resolve => {

      this.db.allDocs({

        include_docs: true,
        limit: 10,
        descending: true

      }).then((result) => {

        this.data = [];

        // let docs = result.rows.map((row) => {
        //   this.data.push(row.doc);
        // });
        result.rows.map((row) => { 
      //    console.log('getDocuments :' + row.doc.toname); 
           if( (row.doc.to == message.toName && row.doc.from == message.from)  || (row.doc.to == message.from && row.doc.from == message.toName)){ 
            this.data.push(row.doc); 
          } 
        });
        this.data.reverse();

        resolve(this.data);

        this.db.changes({ live: true, since: 'now', include_docs: true }).on('change', (change) => {
          this.handleChange(change);
        });

      }).catch((error) => {

        console.log(error);

      });

    });

  }

  // handle if any change in the document
  handleChange(change) {

    let changedDoc = null;
    let changedIndex = null;

    this.data.forEach((doc, index) => {

      if (doc._id === change.id) {
        changedDoc = doc;
        changedIndex = index;
      }

    });

    //A document was deleted
    if (change.deleted) {
      this.data.splice(changedIndex, 1);
    }
    else {

      //A document was updated
      if (changedDoc) {
        this.data[changedIndex] = change.doc;
      }

      //A document was added
      else {
        this.data.push(change.doc);
      }

    }

  }

}
