import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import PouchDB from 'pouchdb';



@Injectable()
export class DriverData {
  //Member Variables
  remote: any;                      //variable for storing remote database address
  db: any;                          //variable for creating database
  data: any;                        //this is used for simply taking data from server
  fix: any;                    //this variable is used to access data in addDrivers function to check for existing driver with same credentials
  cloudantUsername: string;
  cloudantPassword: string;
  options: any;
  constructor(public http: Http) {
    this.db = new PouchDB('DriverList');
    this.cloudantUsername="monlyoulikenuitheysurgen";
    this.cloudantPassword ="27862df06ad91dee729ae33b3c55ec3452e41f74";
    this.remote = 'https://6323cd5c-ab7a-4f85-8e7f-20a6d97305aa-bluemix.cloudant.com/driverdata';
     this.sync();

    this.getAllDrivers();
  }

  /**
   * function to syncronise data
   */

  sync(){
    this.options = {
      live: true,
      retry: true,
      continous: true,
      auth: {
        username: this.cloudantUsername,
        password: this.cloudantPassword
      }
    };
    this.db.sync(this.remote, this.options);

  }

/*
 * retrievs data of all the drivers
 * 
 */

  getAllDrivers() {
    return new Promise(resolve => {

      this.db.allDocs({

        include_docs: true,
        //limit: 30,
        descending: true

      }).then((result) => {
        this.data = [];
        result.rows.map((row) => {
          //console.log("checking row.doc " + row.doc.username);
          if(row.doc.online == true){
          this.data.push(row.doc);
        }
          //console.log(this.data[0].username);
        });

        
        this.fix = this.data;
        resolve(this.data);

        // this.db.changes({ live: true, since: 'now', include_docs: true }).on('change', (change) => {
        //   this.handleChange(change);
        // });

      }).catch((error) => {

        console.log(error);

      });

    });
  }


}
